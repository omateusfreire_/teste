<?php

$despesas[0] = 345.55;
$despesas[1] = 1350.00;
$despesas[2] = 600.00;
$despesas[3] = 900.00;
$despesas[4] = 400.00;

//usando o vetor para apresentar as despesas

for($i = 0; $i < 5; $i++){

    echo $despesas[$i] . "<br>";

}

unset($despesas);//apaguei o vetor de cima

$despesas['mercado'] = 345.55;
$despesas['estacionamento'] = 1350.00;
$despesas['limentação'] = 600.00;
$despesas['bar'] = 900.00;
$despesas['educacao'] = 400.00;

//lupping vetor foreach significa para cada
//

echo "<br>Despesas<br>";


foreach($despesas as $nome /*indice*/ => $gasto /*valor*/ ){

    echo "$nome: R$" .number_format($gasto) . "<br>";

}

echo "<br>o que eu faço nos dias da semana<br>";
echo "<br>";

$semana['segunda'] = "vou para a aula de P.I";
$semana['terça'] = 'vou para a aula de cms e depois para a de direito digital';
$semana['quarta'] = 'vou para a aula de banco de dados';
$semana['quinta'] = 'vou para a aula de linguagem de servidor';
$semana['sexta'] = 'vou para a aula de linguagem de script para web';
$semana['sábado'] = 'coloco em prática o que eu aprendi nas aulas ';
$semana['domingo']= 'almoço com a família';

foreach($semana as $dia => $faco){

    echo "$dia: ". $faco . "<br>";

}
